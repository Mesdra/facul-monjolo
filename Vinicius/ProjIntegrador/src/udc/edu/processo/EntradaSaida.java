package udc.edu.processo;

import udc.edu.Dominio.Processo;
import udc.edu.controle.SO;

public class EntradaSaida {

	public void executaUmaPosicaoFilaEntradaSaida1(SO sistemOperacional) {
		if (sistemOperacional.getEntradaSaida1().tamanho() == 0)
			return;
		Processo processo = sistemOperacional.getEntradaSaida1().consulta();
		sistemOperacional.getEntradaSaida1().remover();
		int i = processo.getPropriedade().getPosica();
		i++;
		processo.getPropriedade().setPosica(i);
		switch (processo.getPropriedade().getPrioridade()) {
		case ALTA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosAltaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;
		case MEDIA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosMediaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;
		case BAIXA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosBaixaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;
		}
	}

	public void executaUmaPosicaoFilaEntradaSaida2(SO sistemOperacional) {
		if (sistemOperacional.getEntradaSaida2().tamanho() == 0)
			return;
		Processo processo = sistemOperacional.getEntradaSaida2().consulta();
		sistemOperacional.getEntradaSaida2().remover();
		int i = processo.getPropriedade().getPosica();
		i++;
		processo.getPropriedade().setPosica(i);
		switch (processo.getPropriedade().getPrioridade()) {
		case ALTA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosAltaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;
		case MEDIA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosMediaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;
		case BAIXA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosBaixaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;

		}
	}

	public void executaUmaPosicaoFilaEntradaSaida3(SO sistemOperacional) {
		if (sistemOperacional.getEntradaSaida3().tamanho() == 0)
			return;
		Processo processo = sistemOperacional.getEntradaSaida3().consulta();
		sistemOperacional.getEntradaSaida3().remover();
		int i = processo.getPropriedade().getPosica();
		i++;
		processo.getPropriedade().setPosica(i);
		switch (processo.getPropriedade().getPrioridade()) {
		case ALTA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosAltaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;
		case MEDIA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosMediaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;
		case BAIXA:
			for (int p = 0; p < sistemOperacional.getListaDeProcessosPausados().tamanho(); p++)
				if (sistemOperacional.getListaDeProcessosPausados().obtem(p).getPropriedade().getPid() == processo
						.getPropriedade().getPid()) {
					sistemOperacional.getFilaExecucaoProcessosBaixaPrioridade()
							.adiciona(sistemOperacional.getListaDeProcessosPausados().obtem(p));
					sistemOperacional.getListaDeProcessosPausados().remove(p);
					break;
				}
			break;
		}
	}

}
