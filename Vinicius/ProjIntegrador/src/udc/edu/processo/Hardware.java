package udc.edu.processo;

import udc.edu.Dominio.Instrucoes;

import udc.edu.controle.SO;

public class Hardware {

	public Instrucoes processamentoCPUPorClock(SO dados) {
		int i = dados.getProcessoEmExecucao().getPropriedade().getPosica();
		int tempo = dados.getProcessoEmExecucao().getEstatistica().getTemDeProcesso();
		tempo++;
		dados.getProcessoEmExecucao().getEstatistica().setTemDeProcesso(tempo);
		tempo = dados.getTempoDoProcesso();
		tempo++;
		dados.setTempoDoProcesso(tempo);
		i++;
		Instrucoes tipoProcesso = dados.getProcessoEmExecucao().getPropriedade().getVetor().obtem(i);
		if (tipoProcesso == Instrucoes.INTRUCAOCPU) {
			dados.getProcessoEmExecucao().getPropriedade().setPosica(i);
			return Instrucoes.INTRUCAOCPU;
		}

		if (tipoProcesso == Instrucoes.FIM) {
			return Instrucoes.FIM;
		}

		if (tipoProcesso == Instrucoes.INTRUCAOENTRADASAIDA1) {
			return Instrucoes.INTRUCAOENTRADASAIDA1;
		}
		if (tipoProcesso == Instrucoes.INTRUCAOENTRADASAIDA2) {
			return Instrucoes.INTRUCAOENTRADASAIDA2;
		}
		if (tipoProcesso == Instrucoes.INTRUCAOENTRADASAIDA3) {
			return Instrucoes.INTRUCAOENTRADASAIDA3;
		}
		return null;
	}
}
