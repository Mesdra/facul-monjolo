package udc.edu.controle;

import java.util.Scanner;

import udc.edu.Dominio.Instrucoes;
import udc.edu.interfac.InterfaceConsole;
import udc.edu.processo.EntradaSaida;
import udc.edu.processo.Hardware;

public class Simulador {

	public static void main(String[] args) {
		EntradaSaida entradasaida = new EntradaSaida();
		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();
		Instrucoes instrucao = null;
		int comando = 0;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		do {
			if (sc.hasNext()) {
				comando = interfaceConsole.iniciaMenu(sistemOperacional);
			}
			if (comando == 1) {
				sistemOperacional.criaProcesso(interfaceConsole);

			}
			if (comando == 2) {
				
			}
			if (sistemOperacional.getProcessoEmExecucao() == null) {
				sistemOperacional.primeiroProcessoAserExecutado();
			}

			if (sistemOperacional.tempoDoProcessoNaCPU() && sistemOperacional.getProcessoEmExecucao() != null) {
				instrucao = hardware.processamentoCPUPorClock(sistemOperacional);

				if (instrucao == Instrucoes.INTRUCAOENTRADASAIDA1 || instrucao == Instrucoes.INTRUCAOENTRADASAIDA2
						|| instrucao == Instrucoes.INTRUCAOENTRADASAIDA3) {
					sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
				}
			} else {
				if(sistemOperacional.getProcessoEmExecucao() != null)
				sistemOperacional.trocaProcessoEmExecucaoProtempo();
			}

			if (instrucao == Instrucoes.FIM && sistemOperacional.getProcessoEmExecucao() != null) {
				sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
			}
			if (instrucao == Instrucoes.PAUSADO) {
				sistemOperacional.pausaProcesso();
			}
			if (sistemOperacional.getProcessoEmExecucao() != null) {
				entradasaida.executaUmaPosicaoFilaEntradaSaida1(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida2(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida3(sistemOperacional);
			}
		} while (comando != -1);

	}

}
