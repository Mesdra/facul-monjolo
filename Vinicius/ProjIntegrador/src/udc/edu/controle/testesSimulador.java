package udc.edu.controle;

import org.junit.Assert;
import org.junit.Test;

import udc.edu.Dominio.Instrucoes;
import udc.edu.Dominio.Prioridade;
import udc.edu.interfac.InterfaceConsole;
import udc.edu.processo.EntradaSaida;
import udc.edu.processo.Hardware;

public class testesSimulador {

	@Test
	public void testeHoraDeFecharOProcessoEHardware() {

		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();

		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(80);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);
		sistemOperacional.primeiroProcessoAserExecutado();
		Assert.assertNotNull(sistemOperacional.getProcessoEmExecucao());
		sistemOperacional.setClockProSegundo(100);

		Assert.assertTrue(sistemOperacional.tempoDoProcessoNaCPU());

		for (int i = 0; i < 60; i++)
			hardware.processamentoCPUPorClock(sistemOperacional);

		Assert.assertFalse(sistemOperacional.tempoDoProcessoNaCPU());
	}

	@Test
	public void testetrocaProcessoProTempo() {

		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();

		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(80);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.BAIXA);
		interfaceConsole.setQuantidadeDeIntrucoes(40);

		sistemOperacional.criaProcesso(interfaceConsole);

		sistemOperacional.primeiroProcessoAserExecutado();
		Assert.assertNotNull(sistemOperacional.getProcessoEmExecucao());
		sistemOperacional.setClockProSegundo(100);

		Assert.assertTrue(sistemOperacional.tempoDoProcessoNaCPU());

		for (int i = 0; i < 60; i++)
			hardware.processamentoCPUPorClock(sistemOperacional);

		sistemOperacional.trocaProcessoEmExecucaoProtempo();

		Assert.assertTrue(sistemOperacional.tempoDoProcessoNaCPU());

		Assert.assertEquals(sistemOperacional.getProcessoEmExecucao().getPropriedade().getPrioridade(),
				Prioridade.BAIXA);
		Assert.assertEquals(sistemOperacional.getTempoDoProcesso(), 0);
	}

	@Test
	public void testeFinalizaProcesso() {

		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();
		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(60);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);
		sistemOperacional.primeiroProcessoAserExecutado();
		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(20);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);
		Assert.assertNotNull(sistemOperacional.getProcessoEmExecucao());
		sistemOperacional.setClockProSegundo(100);

		Assert.assertTrue(sistemOperacional.tempoDoProcessoNaCPU());

		for (int i = 0; i < 59; i++)
			hardware.processamentoCPUPorClock(sistemOperacional);
		Instrucoes instrucaoFim = hardware.processamentoCPUPorClock(sistemOperacional);
		sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucaoFim);
		Assert.assertEquals(sistemOperacional.getProcessoEmExecucao().getPropriedade().getPrioridade(),
				Prioridade.MEDIA);
	}

	@Test
	public void pausaProcessoEmExecucao() {

		SO sistemOperacional = new SO();
		InterfaceConsole interfaceConsole = new InterfaceConsole();

		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(60);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);
		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(60);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);
		sistemOperacional.primeiroProcessoAserExecutado();
		sistemOperacional.pausaProcesso();
		Assert.assertEquals(sistemOperacional.getListaDeProcessosPausados().tamanho(), 1);
		Assert.assertEquals(sistemOperacional.getFilaExecucaoProcessosAltaPrioridade().tamanho(), 0);

	}

	@Test
	public void testeDoProcessamentoEntradaSaida() {

		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();
		EntradaSaida entradasaida = new EntradaSaida();

		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(30);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(1);
		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(40);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);
		sistemOperacional.primeiroProcessoAserExecutado();
		Instrucoes instrucao;
		for (int i = 0; i < 30; i++) {
			instrucao = hardware.processamentoCPUPorClock(sistemOperacional);
			if (instrucao == Instrucoes.INTRUCAOENTRADASAIDA1 || instrucao == Instrucoes.INTRUCAOENTRADASAIDA2
					|| instrucao == Instrucoes.INTRUCAOENTRADASAIDA3) {
				sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
			}
			entradasaida.executaUmaPosicaoFilaEntradaSaida1(sistemOperacional);

		}
		Assert.assertEquals(sistemOperacional.getProcessoEmExecucao().getPropriedade().getPrioridade(),
				Prioridade.MEDIA);

	}

	@Test
	public void testeDoProcessamentoEntradaSaidavariada() {

		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();
		EntradaSaida entradasaida = new EntradaSaida();
		sistemOperacional.setClockProSegundo(100);
		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(5);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(1);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(1);
		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(10);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(1);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);
		sistemOperacional.primeiroProcessoAserExecutado();
		Instrucoes instrucao = null;
		for (int i = 0; i < 20; i++) {
			if (sistemOperacional.tempoDoProcessoNaCPU() && sistemOperacional.getProcessoEmExecucao() != null) {
				instrucao = hardware.processamentoCPUPorClock(sistemOperacional);

				if (instrucao == Instrucoes.INTRUCAOENTRADASAIDA1 || instrucao == Instrucoes.INTRUCAOENTRADASAIDA2
						|| instrucao == Instrucoes.INTRUCAOENTRADASAIDA3) {
					sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
				}
			} else {
				if (sistemOperacional.getProcessoEmExecucao() != null)
					sistemOperacional.trocaProcessoEmExecucaoProtempo();
			}

			if (instrucao == Instrucoes.FIM && sistemOperacional.getProcessoEmExecucao() != null) {
				sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
			}
			if (instrucao == Instrucoes.PAUSADO) {
				sistemOperacional.pausaProcesso();
			}
			if (sistemOperacional.getProcessoEmExecucao() != null) {
				entradasaida.executaUmaPosicaoFilaEntradaSaida1(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida2(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida3(sistemOperacional);
			}
		}
		Assert.assertEquals(sistemOperacional.getListaDeProcessosFinalizados().tamanho(), 2);
		Assert.assertNull(sistemOperacional.getProcessoEmExecucao());

	}

	@Test
	public void TrocaProcessoPorTempo() {

		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();
		EntradaSaida entradasaida = new EntradaSaida();
		sistemOperacional.setClockProSegundo(10);
		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(10);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);
		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(10);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);

		Instrucoes instrucao = null;
		for (int i = 0; i < 50; i++) {
			if (sistemOperacional.getProcessoEmExecucao() == null) {
				sistemOperacional.primeiroProcessoAserExecutado();
			}
			if (sistemOperacional.tempoDoProcessoNaCPU() && sistemOperacional.getProcessoEmExecucao() != null) {
				instrucao = hardware.processamentoCPUPorClock(sistemOperacional);

				if (instrucao == Instrucoes.INTRUCAOENTRADASAIDA1 || instrucao == Instrucoes.INTRUCAOENTRADASAIDA2
						|| instrucao == Instrucoes.INTRUCAOENTRADASAIDA3) {
					sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
				}
			} else {
				if (sistemOperacional.getProcessoEmExecucao() != null)
					sistemOperacional.trocaProcessoEmExecucaoProtempo();
			}

			if (instrucao == Instrucoes.FIM && sistemOperacional.getProcessoEmExecucao() != null) {
				sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
			}
			if (instrucao == Instrucoes.PAUSADO) {
				sistemOperacional.pausaProcesso();
			}
			if (sistemOperacional.getProcessoEmExecucao() != null) {
				entradasaida.executaUmaPosicaoFilaEntradaSaida1(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida2(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida3(sistemOperacional);
			}
		}
		Assert.assertEquals(sistemOperacional.getListaDeProcessosFinalizados().tamanho(), 2);
		Assert.assertNull(sistemOperacional.getProcessoEmExecucao());

	}

	@Test
	public void testandoVariosProcessosAoMesmoTempo() {

		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();
		EntradaSaida entradasaida = new EntradaSaida();
		sistemOperacional.setClockProSegundo(10);
		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(4);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);
		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(5);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);
		interfaceConsole.setPrioridade(Prioridade.BAIXA);
		interfaceConsole.setQuantidadeDeIntrucoes(2);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(20);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(7);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);

		Instrucoes instrucao = null;
		for (int i = 0; i < 250; i++) {
			if (sistemOperacional.getProcessoEmExecucao() == null) {
				sistemOperacional.primeiroProcessoAserExecutado();
			}
			if (sistemOperacional.tempoDoProcessoNaCPU() && sistemOperacional.getProcessoEmExecucao() != null) {
				instrucao = hardware.processamentoCPUPorClock(sistemOperacional);

				if (instrucao == Instrucoes.INTRUCAOENTRADASAIDA1 || instrucao == Instrucoes.INTRUCAOENTRADASAIDA2
						|| instrucao == Instrucoes.INTRUCAOENTRADASAIDA3) {
					sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
				}
			} else {
				if (sistemOperacional.getProcessoEmExecucao() != null)
					sistemOperacional.trocaProcessoEmExecucaoProtempo();
			}

			if (instrucao == Instrucoes.FIM && sistemOperacional.getProcessoEmExecucao() != null) {
				sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
			}
			if (instrucao == Instrucoes.PAUSADO) {
				sistemOperacional.pausaProcesso();
			}
			if (sistemOperacional.getProcessoEmExecucao() != null) {
				entradasaida.executaUmaPosicaoFilaEntradaSaida1(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida2(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida3(sistemOperacional);
			}
		}
		Assert.assertEquals(sistemOperacional.getListaDeProcessosFinalizados().tamanho(), 5);
		Assert.assertNull(sistemOperacional.getProcessoEmExecucao());

	}

	@Test
	public void testandoVariosProcessosAoMesmoTempoComEntadasSaidas() {

		SO sistemOperacional = new SO();
		Hardware hardware = new Hardware();
		InterfaceConsole interfaceConsole = new InterfaceConsole();
		EntradaSaida entradasaida = new EntradaSaida();
		sistemOperacional.setClockProSegundo(10);
		interfaceConsole.setMemoria(6);
		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(4);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(1);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);
		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(5);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(2);

		sistemOperacional.criaProcesso(interfaceConsole);
		interfaceConsole.setPrioridade(Prioridade.BAIXA);
		interfaceConsole.setQuantidadeDeIntrucoes(2);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(20);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(0);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(5);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(0);

		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.MEDIA);
		interfaceConsole.setQuantidadeDeIntrucoes(7);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(1);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(1);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(1);

		sistemOperacional.criaProcesso(interfaceConsole);

		interfaceConsole.setPrioridade(Prioridade.ALTA);
		interfaceConsole.setQuantidadeDeIntrucoes(70);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida2(6);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida3(5);
		interfaceConsole.setQuantidadeDeIntrucaoEntradaEsaida1(2);

		sistemOperacional.criaProcesso(interfaceConsole);

		Instrucoes instrucao = null;
		for (int i = 0; i < 250; i++) {
			if (sistemOperacional.getProcessoEmExecucao() == null) {
				sistemOperacional.primeiroProcessoAserExecutado();
			}
			if (sistemOperacional.tempoDoProcessoNaCPU() && sistemOperacional.getProcessoEmExecucao() != null) {
				instrucao = hardware.processamentoCPUPorClock(sistemOperacional);

				if (instrucao == Instrucoes.INTRUCAOENTRADASAIDA1 || instrucao == Instrucoes.INTRUCAOENTRADASAIDA2
						|| instrucao == Instrucoes.INTRUCAOENTRADASAIDA3) {
					sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
				}
			} else {
				if (sistemOperacional.getProcessoEmExecucao() != null)
					sistemOperacional.trocaProcessoEmExecucaoProtempo();
			}

			if (instrucao == Instrucoes.FIM && sistemOperacional.getProcessoEmExecucao() != null) {
				sistemOperacional.trocaProcessoEmExecucaoPorUmaCondicao(instrucao);
			}
			if (instrucao == Instrucoes.PAUSADO) {
				sistemOperacional.pausaProcesso();
			}
			if (sistemOperacional.getProcessoEmExecucao() != null) {
				entradasaida.executaUmaPosicaoFilaEntradaSaida1(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida2(sistemOperacional);
				entradasaida.executaUmaPosicaoFilaEntradaSaida3(sistemOperacional);
			}
		}
		Assert.assertEquals(sistemOperacional.getListaDeProcessosFinalizados().tamanho(), 6);
		Assert.assertNull(sistemOperacional.getProcessoEmExecucao());

	}
}
