package udc.edu.controle;



import br.edu.udc.ed.vetor.Vetor;
import udc.edu.Dominio.ContextoDeSof;
import udc.edu.Dominio.Estatistica;
import udc.edu.Dominio.Fila;
import udc.edu.Dominio.Instrucoes;
import udc.edu.Dominio.Memoria;
import udc.edu.Dominio.Prioridade;
import udc.edu.Dominio.Processo;
import udc.edu.interfac.InterfaceConsole;


public class SO {
	private int pid = 500;
	private Fila<Processo> entradaSaida1 = new Fila<>();
	private Fila<Processo> entradaSaida2 = new Fila<>();
	private Fila<Processo> entradaSaida3 = new Fila<>();
	private Fila<Processo> filaExecucaoProcessosBaixaPrioridade = new Fila<>();
	private Fila<Processo> filaExecucaoProcessosMediaPrioridade = new Fila<>();
	private Fila<Processo> filaExecucaoProcessosAltaPrioridade = new Fila<>();
	private Vetor<Processo> listaDeProcessosPausados = new Vetor<>();
	private Vetor<Processo> listaDeProcessosFinalizados = new Vetor<>();
	private Processo processoEmExecucao;
	private int tempoDoProcesso;
	private int clockProSegundo = 100;

	public Processo criaProcesso(InterfaceConsole processo) {
		final Processo processoNovo = new Processo();
		final ContextoDeSof contexto = new ContextoDeSof();
		final Estatistica estatisticas = new Estatistica();
		int quantidadeDeIntrucoes = 0;

		estatisticas.setEstado(0);
		estatisticas.setTemDeProcesso(0);
		for (int i = 0; i < processo.getQuantidadeDeIntrucoes(); i++) {
			contexto.getVetor().adicionar(Instrucoes.INTRUCAOCPU);
		}
		contexto.setPid(pid);
		contexto.setPrioridade(processo.getPrioridade());
		final Memoria memor = new Memoria();
		memor.setConsumo(processo.getMemoria());
		processoNovo.setMemoria(memor);
		pid++;
		if (processo.getQuantidadeDeIntrucaoEntradaEsaida1() != 0) {
			quantidadeDeIntrucoes = processo.getQuantidadeDeIntrucaoEntradaEsaida1();
			do {
				contexto.getVetor().adicionar(Instrucoes.INTRUCAOENTRADASAIDA1);
				quantidadeDeIntrucoes--;
			} while (quantidadeDeIntrucoes != 0);
		}

		if (processo.getQuantidadeDeIntrucaoEntradaEsaida2() != 0) {
			quantidadeDeIntrucoes = processo.getQuantidadeDeIntrucaoEntradaEsaida2();
			do {
				contexto.getVetor().adicionar(Instrucoes.INTRUCAOENTRADASAIDA2);
				quantidadeDeIntrucoes--;
			} while (quantidadeDeIntrucoes != 0);
		}

		if (processo.getQuantidadeDeIntrucaoEntradaEsaida3() != 0) {
			quantidadeDeIntrucoes = processo.getQuantidadeDeIntrucaoEntradaEsaida3();
			do {
				contexto.getVetor().adicionar(Instrucoes.INTRUCAOENTRADASAIDA3);
				quantidadeDeIntrucoes--;
			} while (quantidadeDeIntrucoes != 0);
		}
		contexto.getVetor().embaralhaVetor();
		contexto.getVetor().adicionar(Instrucoes.FIM);
		processoNovo.setPropriedade(contexto);
		processoNovo.setEstatistica(estatisticas);
		if (processoNovo.getPropriedade().getPrioridade() == Prioridade.BAIXA) {

			filaExecucaoProcessosBaixaPrioridade.adiciona(processoNovo);

		}
		if (processoNovo.getPropriedade().getPrioridade() == Prioridade.MEDIA) {

			filaExecucaoProcessosMediaPrioridade.adiciona(processoNovo);

		}
		if (processoNovo.getPropriedade().getPrioridade() == Prioridade.ALTA) {

			filaExecucaoProcessosAltaPrioridade.adiciona(processoNovo);

		}
		return processoNovo;
	}

	public Fila<Processo> getEntradaSaida1() {
		return entradaSaida1;
	}

	public void setEntradaSaida1(Fila<Processo> entradaSaida1) {
		this.entradaSaida1 = entradaSaida1;
	}

	public Fila<Processo> getEntradaSaida2() {
		return entradaSaida2;
	}

	public void setEntradaSaida2(Fila<Processo> entradaSaida2) {
		this.entradaSaida2 = entradaSaida2;
	}

	public Fila<Processo> getEntradaSaida3() {
		return entradaSaida3;
	}

	public void setEntradaSaida3(Fila<Processo> entradaSaida3) {
		this.entradaSaida3 = entradaSaida3;
	}

	public Vetor<Processo> getListaDeProcessosFinalizados() {
		return listaDeProcessosFinalizados;
	}

	public void setListaDeProcessosFinalizados(Vetor<Processo> listaDeProcessosFinalizados) {
		this.listaDeProcessosFinalizados = listaDeProcessosFinalizados;
	}

	public int getTempoDoProcesso() {
		return tempoDoProcesso;
	}

	public void setTempoDoProcesso(int tempoDoProcesso) {
		this.tempoDoProcesso = tempoDoProcesso;
	}

	public int getClockProSegundo() {
		return clockProSegundo;
	}

	public void setClockProSegundo(int clockProSegundo) {
		this.clockProSegundo = clockProSegundo;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public Vetor<Processo> getListaDeProcessosPausados() {
		return listaDeProcessosPausados;
	}

	public void setListaDeProcessosPausados(Vetor<Processo> listaDeProcessosPausados) {
		this.listaDeProcessosPausados = listaDeProcessosPausados;
	}

	public Processo getProcessoEmExecucao() {
		return processoEmExecucao;
	}

	public void setProcessoEmExecucao(Processo processoEmExecucao) {
		this.processoEmExecucao = processoEmExecucao;
	}

	public Fila<Processo> getFilaExecucaoProcessosBaixaPrioridade() {
		return filaExecucaoProcessosBaixaPrioridade;
	}

	public void setFilaExecucaoProcessosBaixaPrioridade(Fila<Processo> filaExecucaoProcessosBaixaPrioridade) {
		this.filaExecucaoProcessosBaixaPrioridade = filaExecucaoProcessosBaixaPrioridade;
	}

	public Fila<Processo> getFilaExecucaoProcessosMediaPrioridade() {
		return filaExecucaoProcessosMediaPrioridade;
	}

	public void setFilaExecucaoProcessosMediaPrioridade(Fila<Processo> filaExecucaoProcessosMediaPrioridade) {
		this.filaExecucaoProcessosMediaPrioridade = filaExecucaoProcessosMediaPrioridade;
	}

	public Fila<Processo> getFilaExecucaoProcessosAltaPrioridade() {
		return filaExecucaoProcessosAltaPrioridade;
	}

	public void setFilaExecucaoProcessosAltaPrioridade(Fila<Processo> filaExecucaoProcessosAltaPrioridade) {
		this.filaExecucaoProcessosAltaPrioridade = filaExecucaoProcessosAltaPrioridade;
	}

	public boolean tempoDoProcessoNaCPU() {
		final int altaPrioridade = (clockProSegundo * 60) / 100;
		final int mediaPrioridade = (clockProSegundo * 30) / 100;
		final int baixaPrioridade = (clockProSegundo * 10) / 100;
		if(this.processoEmExecucao != null){
		switch (this.processoEmExecucao.getPropriedade().getPrioridade()) {

		case ALTA:
			if (this.tempoDoProcesso < altaPrioridade) {
				return true;
			}
			return false;
		case MEDIA:
			if (this.tempoDoProcesso < mediaPrioridade) {
				return true;
			}
			return false;
		case BAIXA:
			if (this.tempoDoProcesso < baixaPrioridade) {
				return true;
			}
			return false;
		}
		}
		return false;
	}

	public void adicionalistadepause() {
		// TODO Auto-generated method stub

	}

	public void adicionaProcessoListaDeEntradaSaida(Instrucoes instrucao) {
		// TODO Auto-generated method stub

	}

	public void primeiroProcessoAserExecutado() {
		if (this.processoEmExecucao == null) {
			if (this.filaExecucaoProcessosAltaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosAltaPrioridade.consulta();
				this.filaExecucaoProcessosAltaPrioridade.remover();
				return;
			}
			if (this.filaExecucaoProcessosMediaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosMediaPrioridade.consulta();
				this.filaExecucaoProcessosMediaPrioridade.remover();
				return;
			}
			if (this.filaExecucaoProcessosBaixaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosBaixaPrioridade.consulta();
				this.filaExecucaoProcessosBaixaPrioridade.remover();
				return;
			}
			this.processoEmExecucao= null;

		}
	}

	public void ProcessoTrocadoPassandoUmaPrioridade(Prioridade prioridade) {
		switch (prioridade) {
		case ALTA:
			this.processoEmExecucao = this.filaExecucaoProcessosAltaPrioridade.consulta();
			this.filaExecucaoProcessosAltaPrioridade.remover();
			break;
		case MEDIA:
			this.processoEmExecucao = this.filaExecucaoProcessosMediaPrioridade.consulta();
			this.filaExecucaoProcessosMediaPrioridade.remover();
			break;
		case BAIXA:
			this.processoEmExecucao = this.filaExecucaoProcessosBaixaPrioridade.consulta();
			this.filaExecucaoProcessosBaixaPrioridade.remover();
			break;
		}

	}

	public void trocaProcessoEmExecucaoProtempo() {
		this.tempoDoProcesso = 0;
		if (this.processoEmExecucao.getPropriedade().getPrioridade() == Prioridade.ALTA) {
			this.filaExecucaoProcessosAltaPrioridade.adiciona(this.processoEmExecucao);
			if (this.filaExecucaoProcessosMediaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosMediaPrioridade.consulta();
				this.filaExecucaoProcessosMediaPrioridade.remover();
				return;
			}
			if (this.filaExecucaoProcessosBaixaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosBaixaPrioridade.consulta();
				this.filaExecucaoProcessosBaixaPrioridade.remover();
				return;
			}
			this.processoEmExecucao = null;
			return;
		}
		if (this.processoEmExecucao.getPropriedade().getPrioridade() == Prioridade.MEDIA) {
			this.filaExecucaoProcessosMediaPrioridade.adiciona(this.processoEmExecucao);
			if (this.filaExecucaoProcessosBaixaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosBaixaPrioridade.consulta();
				this.filaExecucaoProcessosBaixaPrioridade.remover();
				return;
			}
			if (this.filaExecucaoProcessosAltaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosAltaPrioridade.consulta();
				this.filaExecucaoProcessosAltaPrioridade.remover();
				return;
			}
			this.processoEmExecucao = null;
			return;
		}

		if (this.processoEmExecucao.getPropriedade().getPrioridade() == Prioridade.BAIXA) {
			this.filaExecucaoProcessosBaixaPrioridade.adiciona(this.processoEmExecucao);
			if (this.filaExecucaoProcessosAltaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosAltaPrioridade.consulta();
				this.filaExecucaoProcessosAltaPrioridade.remover();
				return;
			}
			if (this.filaExecucaoProcessosMediaPrioridade.tamanho() != 0) {
				this.processoEmExecucao = this.filaExecucaoProcessosMediaPrioridade.consulta();
				this.filaExecucaoProcessosMediaPrioridade.remover();
				return;
			}
			this.processoEmExecucao = null;
			return;
		}
		
	}

	public void finalizaProcessoExecucao() {
		for(int i =0;i<this.listaDeProcessosFinalizados.tamanho();i++)
			if(this.processoEmExecucao == this.listaDeProcessosFinalizados.obtem(i))
				return;
		this.listaDeProcessosFinalizados.adicionar(this.processoEmExecucao);

	}

	public void pausaProcesso() {
		this.listaDeProcessosPausados.adicionar(this.processoEmExecucao);
		if(verificaExistenciDeProcessoNoSistema()){
		Prioridade prioridade = this.pegarProximaFilaNaoVaziadoProcessoEmExecucao();
		this.ajustaClockDoSistema(prioridade);
		this.ProcessoTrocadoPassandoUmaPrioridade(prioridade);
		}
	}

	@SuppressWarnings("incomplete-switch")
	public void trocaProcessoEmExecucaoPorUmaCondicao(Instrucoes instrucao) {
		Prioridade prioridade ;
		switch (instrucao) {
// arrumar o verificaprocesso
		case FIM:
			this.finalizaProcessoExecucao();
			if(verificaExistenciDeProcessoNoSistema()){
			prioridade = this.pegarProximaFilaNaoVaziadoProcessoEmExecucao();
			this.ajustaClockDoSistema(prioridade);
			this.ProcessoTrocadoPassandoUmaPrioridade(prioridade);
			}else 
				this.processoEmExecucao = null;
			break;
		case INTRUCAOENTRADASAIDA1:
			this.entradaSaida1.adiciona(this.processoEmExecucao);
			this.pausaProcesso();
			break;
		case INTRUCAOENTRADASAIDA2:
			this.entradaSaida2.adiciona(this.processoEmExecucao);
			this.pausaProcesso();
			break;
		case INTRUCAOENTRADASAIDA3:
			this.entradaSaida3.adiciona(this.processoEmExecucao);
			this.pausaProcesso();
			break;
		case PAUSADO:
			this.pausaProcesso();
			break;
		}

	}

	private void ajustaClockDoSistema(Prioridade prioridade) {
		if(this.processoEmExecucao.getPropriedade().getPrioridade() == prioridade){
			return;
		}else{
			this.tempoDoProcesso = 0;
		}
		
	}

	private Prioridade pegarProximaFilaNaoVaziadoProcessoEmExecucao() {
		if (this.processoEmExecucao.getPropriedade().getPrioridade() == Prioridade.ALTA) {
			if (this.filaExecucaoProcessosAltaPrioridade.tamanho() != 0) {
				return Prioridade.ALTA;
			}
			if (this.filaExecucaoProcessosMediaPrioridade.tamanho() != 0) {
				return Prioridade.MEDIA;
			}
			if (this.filaExecucaoProcessosBaixaPrioridade.tamanho() != 0) {
				return Prioridade.BAIXA;
			}
			return null;
		}
		if (this.processoEmExecucao.getPropriedade().getPrioridade() == Prioridade.MEDIA) {
			if (this.filaExecucaoProcessosMediaPrioridade.tamanho() != 0) {
				return Prioridade.MEDIA;
			}
			if (this.filaExecucaoProcessosBaixaPrioridade.tamanho() != 0) {
				return Prioridade.BAIXA;
			}
			if (this.filaExecucaoProcessosAltaPrioridade.tamanho() != 0) {
				return Prioridade.ALTA;
			}
			return null;

		}

		if (this.processoEmExecucao.getPropriedade().getPrioridade() == Prioridade.BAIXA) {
			if (this.filaExecucaoProcessosBaixaPrioridade.tamanho() != 0) {
				return Prioridade.BAIXA;
			}
			if (this.filaExecucaoProcessosAltaPrioridade.tamanho() != 0) {
				return Prioridade.ALTA;
			}
			if (this.filaExecucaoProcessosMediaPrioridade.tamanho() != 0) {
				return Prioridade.MEDIA;
			}
			return null;
		}
		return null;
	}
	public boolean verificaExistenciDeProcessoNoSistema(){
		if(this.filaExecucaoProcessosAltaPrioridade.tamanho() != 0)
			return true;
		if(this.filaExecucaoProcessosMediaPrioridade.tamanho() != 0)
			return true;
		if(this.filaExecucaoProcessosBaixaPrioridade.tamanho() != 0)
			return true;
		return false;
	}

}
