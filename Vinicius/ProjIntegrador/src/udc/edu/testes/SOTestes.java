package udc.edu.testes;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import udc.edu.Dominio.EntradaSaida;
import udc.edu.Dominio.Fila;
import udc.edu.Dominio.Instrucoes;
import udc.edu.Dominio.Prioridade;
import udc.edu.Dominio.Processo;
import udc.edu.controle.SO;
import udc.edu.interfac.InterfaceConsole;

public class SOTestes {

	@Test
	public void criarProcessoSemIOComPrioridadeBaixa() {
		final SO so = new SO();
		final InterfaceConsole processoNovo = new InterfaceConsole();
		processoNovo.setQuantidadeDeIntrucoes(20);
		processoNovo.setMemoria(5);
		processoNovo.setPrioridade(Prioridade.BAIXA);
		
		final Processo processo = so.criaProcesso(processoNovo);
		
		final Processo processo2 = so.criaProcesso(processoNovo);

		Assert.assertNotNull(processo);
		Assert.assertNotNull(processo2);
		Assert.assertNotNull(processo.getPId());
		Assert.assertNotNull(processo2.getPId());
		Assert.assertEquals(2, so.getFilaExecucaoProcessosBaixaPrioridade().tamanho());
	}

	@Test
	public void criarProcessoComUmaIOComPrioridadeBaixa() {
		final SO so = new SO();
		final InterfaceConsole processoNovo = new InterfaceConsole();
		processoNovo.setQuantidadeDeIntrucoes(20);
		processoNovo.setMemoria(5);
		processoNovo.setPrioridade(Prioridade.BAIXA);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida1(5);
		
		Processo processo = so.criaProcesso(processoNovo);
		Assert.assertNotNull(processo);
		Assert.assertNotNull(processo.getPId());
		Assert.assertEquals(processo.contagemDeEntradasEsaidas(Instrucoes.INTRUCAOENTRADASAIDA1), 5);

	}

	@Test
	public void criarProcessoComduasIOComPrioridadeBaixa() {
		final SO so = new SO();
		final InterfaceConsole processoNovo = new InterfaceConsole();
		processoNovo.setQuantidadeDeIntrucoes(20);
		processoNovo.setMemoria(5);
		processoNovo.setPrioridade(Prioridade.BAIXA);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida1(2);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida2(2);
		
		Processo processo = so.criaProcesso(processoNovo);
		Assert.assertNotNull(processo.getPropriedade());
		Assert.assertNotNull(processo.getPId());
		Assert.assertNotNull(so.getFilaExecucaoProcessosBaixaPrioridade());
		Assert.assertEquals(1, so.getFilaExecucaoProcessosBaixaPrioridade().tamanho());
		Assert.assertEquals(processo.contagemDeEntradasEsaidas(Instrucoes.INTRUCAOENTRADASAIDA1), 2);
		Assert.assertEquals(processo.contagemDeEntradasEsaidas(Instrucoes.INTRUCAOENTRADASAIDA2), 2);

	}

	@Test
	public void criarProcessoComduasIOComPrioridademMedia() {
		final SO so = new SO();
		final InterfaceConsole processoNovo = new InterfaceConsole();
		processoNovo.setQuantidadeDeIntrucoes(20);
		processoNovo.setMemoria(5);
		processoNovo.setPrioridade(Prioridade.MEDIA);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida1(7);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida2(6);
		
		Processo processo = so.criaProcesso(processoNovo);
		Assert.assertNotNull(processo);
		Assert.assertNotNull(processo.getPId());
		Assert.assertNotNull(so.getFilaExecucaoProcessosMediaPrioridade());
		Assert.assertEquals(1, so.getFilaExecucaoProcessosMediaPrioridade().tamanho());
		Assert.assertEquals(processo.contagemDeEntradasEsaidas(Instrucoes.INTRUCAOENTRADASAIDA1), 7);
		Assert.assertEquals(processo.contagemDeEntradasEsaidas(Instrucoes.INTRUCAOENTRADASAIDA2), 6);

	}

	@Test
	public void PausaProcesso() {
		final SO so = new SO();
		final InterfaceConsole processoNovo = new InterfaceConsole();
		processoNovo.setQuantidadeDeIntrucoes(20);
		processoNovo.setMemoria(5);
		processoNovo.setPrioridade(Prioridade.BAIXA);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida1(2);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida2(2);
		
		Processo processo = so.criaProcesso(processoNovo);
		//processo = so.pausaProcesso(pid);
		int posicao = processo.posicaoAtualVetorProcesso();
		Assert.assertEquals(posicao, 20);
	}

	@Test
	public void reiniciaProcesso() {
		final SO so = new SO();
		final InterfaceConsole processoNovo = new InterfaceConsole();
		processoNovo.setQuantidadeDeIntrucoes(20);
		processoNovo.setMemoria(5);
		processoNovo.setPrioridade(Prioridade.BAIXA);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida1(2);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida2(2);
		
		Processo processo = so.criaProcesso(processoNovo);
		
		//processo = so.pausaProcesso(posicaoAtualNo, pid);
		int posicao = processo.posicaoAtualVetorProcesso();

		//processo = so.reiniciaProcesso(posicaoAtualNo, pid);

		Assert.assertNotEquals(processo.posicaoAtualVetorProcesso(), posicao);

	}

	@Test
	public void finalizaProcesso() {
		final SO so = new SO();
		final InterfaceConsole processoNovo = new InterfaceConsole();
		processoNovo.setQuantidadeDeIntrucoes(20);
		processoNovo.setMemoria(5);
		processoNovo.setPrioridade(Prioridade.BAIXA);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida1(2);
		processoNovo.setQuantidadeDeIntrucaoEntradaEsaida2(2);
		
		Processo processo = so.criaProcesso(processoNovo);
	//	processo = so.pausaProcesso(posicaoAtualNo, pid);
		int posicao = processo.posicaoAtualVetorProcesso();

		//processo = so.finalizaProcesso(posicaoAtualNo, pid);

		Assert.assertEquals(processo.posicaoAtualVetorProcesso(), posicao);

	}
}
