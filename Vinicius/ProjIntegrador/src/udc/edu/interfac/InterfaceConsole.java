package udc.edu.interfac;

import java.util.Scanner;


import udc.edu.Dominio.Prioridade;

import udc.edu.controle.SO;
public class InterfaceConsole {
	private int quantidadeDeIntrucoes;
	private int memoria;
	private Prioridade prioridade;
	private int quantidadeDeIntrucaoEntradaEsaida1;
	private int quantidadeDeIntrucaoEntradaEsaida2;
	private int quantidadeDeIntrucaoEntradaEsaida3;
	private int prioridadeNumerica;
	@SuppressWarnings("unused")
	private int pidParaMetodosDoProcesso;
	public int iniciaMenu(SO sistemaoperacional) {
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("***************menu****************" );
		System.out.println("1 adiciona Processo " );
		System.out.println("2 pausa processo " );
		System.out.println("3 mata processo " );
		System.out.println("4 continuar o simulador" );
		System.out.println("***********************************" );
		
		switch(sc.nextInt()){
			case 1:
				System.out.println("Digite A quantidade De instrucoes" );
				this.quantidadeDeIntrucoes = sc.nextInt();
				System.out.println("Digite a quantidade de memoria utilizada no sistema" );
				this.memoria = sc.nextInt();
				System.out.println("digite a prioridade do Sistema (1 = baixa,2 = media,3 = alta)" );
				this.prioridadeNumerica = sc.nextInt();
				if(prioridadeNumerica == 1)
					this.prioridade = Prioridade.BAIXA;
				if(prioridadeNumerica == 2)
					this.prioridade = Prioridade.MEDIA;
				if(prioridadeNumerica == 3)
					this.prioridade = Prioridade.ALTA;
				System.out.println("Digite A quantidade De instrucoes De E/S 1" );
				this.quantidadeDeIntrucaoEntradaEsaida1 = sc.nextInt();
				System.out.println("Digite A quantidade De instrucoes De E/S 2" );
				this.quantidadeDeIntrucaoEntradaEsaida2 = sc.nextInt();
				System.out.println("Digite A quantidade De instrucoes De E/S 3" );
				this.quantidadeDeIntrucaoEntradaEsaida3 = sc.nextInt();
				return 1;
			case 2 : 
				System.out.println(sistemaoperacional.getFilaExecucaoProcessosAltaPrioridade());
				System.out.println(sistemaoperacional.getFilaExecucaoProcessosBaixaPrioridade());
				System.out.println(sistemaoperacional.getFilaExecucaoProcessosMediaPrioridade());
				System.out.println("digite o pid do processo em que deseja ser pausado" );
				this.pidParaMetodosDoProcesso = sc.nextInt();
				return 2;
			case 3 :
				System.out.println(sistemaoperacional.getFilaExecucaoProcessosAltaPrioridade());
				System.out.println(sistemaoperacional.getFilaExecucaoProcessosBaixaPrioridade());
				System.out.println(sistemaoperacional.getFilaExecucaoProcessosMediaPrioridade());
				System.out.println("digite o pid do processo para matalo" );
				this.pidParaMetodosDoProcesso = sc.nextInt();
				return 3;
			case 4 :
				return 0;
			
		}
		return 0;
	}
	public int getQuantidadeDeIntrucoes() {
		return quantidadeDeIntrucoes;
	}
	public void setQuantidadeDeIntrucoes(int quantidadeDeIntrucoes) {
		this.quantidadeDeIntrucoes = quantidadeDeIntrucoes;
	}
	public int getMemoria() {
		return memoria;
	}
	public void setMemoria(int memoria) {
		this.memoria = memoria;
	}
	public Prioridade getPrioridade() {
		return prioridade;
	}
	public void setPrioridade(Prioridade prioridade) {
		this.prioridade = prioridade;
	}
	public int getQuantidadeDeIntrucaoEntradaEsaida1() {
		return quantidadeDeIntrucaoEntradaEsaida1;
	}
	public void setQuantidadeDeIntrucaoEntradaEsaida1(int quantidadeDeIntrucaoEntradaEsaida1) {
		this.quantidadeDeIntrucaoEntradaEsaida1 = quantidadeDeIntrucaoEntradaEsaida1;
	}
	public int getQuantidadeDeIntrucaoEntradaEsaida2() {
		return quantidadeDeIntrucaoEntradaEsaida2;
	}
	public void setQuantidadeDeIntrucaoEntradaEsaida2(int quantidadeDeIntrucaoEntradaEsaida2) {
		this.quantidadeDeIntrucaoEntradaEsaida2 = quantidadeDeIntrucaoEntradaEsaida2;
	}
	public int getQuantidadeDeIntrucaoEntradaEsaida3() {
		return quantidadeDeIntrucaoEntradaEsaida3;
	}
	public void setQuantidadeDeIntrucaoEntradaEsaida3(int quantidadeDeIntrucaoEntradaEsaida3) {
		this.quantidadeDeIntrucaoEntradaEsaida3 = quantidadeDeIntrucaoEntradaEsaida3;
	}
	



}
