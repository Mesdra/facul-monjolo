package udc.edu.descricao;

public class DescricaoDoTrabalho {

	/*
	Resumo
	Descri��o do trablho integrador em classe separada.
	*/
	
	/*
	Pacote Controle
	
	- Simulador - Seria o main do programa, onde chama todas as demais instru��es (Classes) inclusive as do SO.
	- SO - Puxa as demais classes para execu��o dos processos propostos, ou seja, aux�lia de forma direta na execu��o completa do programa.
	- testesSimulador - Classe respons�vel por executar todos os testes relacionados � classe Simulador que � respons�vel pela execu��o do programa em si.
	
	 Pacote Dom�nio
	 
	- Contexto de Software - � a especifica��o dos limites e caracter�sticas dos recursos que podem ser alocados pelo processo. � dividido entre: Identifica��o, quotas e privil�gios. Na aba do programa feito (ContextoDeSof) � onde identificamos e definimos  a prioridade de execu��o de determinado comando onde tamb�m define o tamanho e a posi��o em um determinado vetor.A prioridade do processo foi definida como 0 para baixa,1 m�dia e 2 para alta.
	- EntradaSaida - Tipos de entradas dispon�veis para processamento.
	- Estat�stica - Dados dos processos que est�o sendo executados ficam registrados nesta classe para registros e compara��es de informa��es.
	- Fila - E do tipo T pois � generico serve para onde for preciso de uma fila  na estrutura de media baixa media e alta prioridade por exemplo � utilizada, os dados ficarem em ordem na Fila pois com o proximo e anterior, � possivel adicionar e remover dados da fila apenas o primeiro e ultimo respeitando uma fila.
	- Instru��es - Pega todos os comandos do processo que est�o organizados num vetor visto que o �ltimo comando � do fim, para terminar o processo.
	- Mem�ria - Classe respons�vel pela vari�vel que recebe o tanto de mem�ria setado pelo usu�rio.
	- Prioridade - A classe Prioridade est� diretamente ligada com o Contexto de Software, uma vez que a prioridade ser� colocada de acordo com o n�mero setado, 0, 1 ou 2. De acordo com oque foi definido na cria��o dos vetores descritos anteriormente no Contexto de Software.
	- Processo - Classe que possui o conte�do que ser� processado pelas demais classes respons�veis.	
	
	Pacote Interfac
	
	- InterfaceConsole - Classe respons�vel por coletar os dados do usu�rio.
	
	Pacote Processo
	
	- Hardware - Seria a implementa��o onde os processos se alternam na utiliza��o da UCP. Os processos podem ser interrompidos e, posteriomente , restaurados.
		         Na classe criada no programa, � mostrado os estados do processo, visto que podem ser interrompidos e posteriormente restaurados para continua��o do processamento. Os comando instruem nas 
		         fases de determinado processo direcionando para uma sa�da espec�fica, para an�lise etc.
		         
	- EntradaSa�da - Respons�vel pelo Start do processo at� que ele finalize por algum motivo sendo o processo de baixa, m�dia ou alta prioridade.
	
	Pacote Testes
	
	- SOTestes - Classe respons�vel por efetuar testes do sistema para adicionar processos.
	
	
	
	
Descri��o do trablho integrador conforme topicos do livro.

*5.2 - Estrutura do Processo - Est� dividido nas classes do pacote Dominio s�o ContextoDeSof, EntradaSaida, Estatistica, Fila, Instrucoes, Memoria, Prioridade, Processo
Depois passa pelas partes de controle 


*5.2.1 - Contexto de Hardware, Consiste em substituir o contexo de hardware de um processo por outro, mostra em que status est� o processo, est� representado como fila de Pause

*5.2.2 - Contexto de Software, est� estruturado em  Identifica��o, Quotas Privilegios, A parte de Identifica��o, � o PID dentro do ContextodeSoftwareuanto � apenas uma v�riavel que so ocupa um valor em memoria (proximo semestre vamos abordar mais sobre o assunto)

*5.3 - Estado do processo, existe uma classe ENUM chamado Instru��es dentro do pacote Dominio, onde mostra as op��es que ser�o enviadas em formato numero, porem com o nome entrada, as op��es s�o:
INTRUCAOCPU,INTRUCAOENTRADASAIDA1,INTRUCAOENTRADASAIDA2,INTRUCAOENTRADASAIDA3,PAUSADO,FIM

*5.4 - Mudan�a de estado de processo est� implementando dentro da classe simulador dentro do pacote Controle, ele chama as fun�oes da classe SO, � a parte em que  troca um processo em execu��o por uma condi��o(um if) dependendo do estado ele executa uma a��o, exemplo se quiser fanalizar o processo ele passa uma opera��o de fim. se quiser pode voltar pra fila passando a instru��o de entrada e saida 1, ele pausa e manda para a fila de entrada e saida.

*5.5 - Cria��o e Elimina��o de Processo
Tem a classe InterfaceConsole dentro do pacote interface, ao inves de mandar dados primitivos para o simulador, ele manda os dados do tipo inteiros, para mandar para o simulador, dentro do swit case, que pega as informa��es primitivas dentro da classe simulador, onde o simulador chama o metodo criaprocesso, e passa para uma fila de pronto para execu��o. o processo pode ser finalizado, precisa passar o PID , ai o processo que est� em execu��o � comparado, se for igual ao PID � finalizado.

*5.6 - Processo CPU-bond e I/O-bound - Os processos s�o classificados como processamento ou Entrada e Saida, Quando o processo acha ama instru�a� de entrada e saida, ele volta para o simulador que chama um metodo, que troca o processo em execu�o por uma condi��o, e agora � uma instru��o de entrada e saida  passa pelo metodo, e volta pra fila. 

*5.8 - Forma de cria��o de processo. O usu�rio passa todos os dados do processo, � chamado o criaProcesso para adicinar um metodo, dentro do sistema.

*5.9 - Processos Independentes, Subprocessos e Threads. N�o tem no trabalho 

*5.11 - Sinais , s�o as Interrup��es, o usu�rio manda uma instru��o pela interface que entra no simulador, que chama um metodo do SO que realiza a interrup��o, ou finaliza��o do processo, dependendo do que o usu�rio colocar. 

	 */
}
