package udc.edu.Dominio;

public class Processo {
	private ContextoDeSof propriedade;
	private Estatistica estatistica;
	private Memoria memoria;
	

	public ContextoDeSof getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(ContextoDeSof propriedade) {
		this.propriedade = propriedade;
	}

	public Estatistica getEstatistica() {
		return estatistica;
	}

	public void setEstatistica(Estatistica estatistica) {
		this.estatistica = estatistica;
	}

	public Memoria getMemoria() {
		return memoria;
	}

	public void setMemoria(Memoria memoria) {
		this.memoria = memoria;
	}

	public int getPId() {
		return this.propriedade.getPid();
	}

	public int contagemDeEntradasEsaidas(Instrucoes tipoDeEntrada) {
		int contador = 0;
		for (int i = 0; i < this.getPropriedade().getVetor().tamanho(); i++) {
			if (propriedade.getVetor().obtem(i) == tipoDeEntrada) {
				contador++;
			}
		}
		return contador;
	}

	public int posicaoAtualVetorProcesso() {
		// TODO Auto-generated method stub
		return 0;
	}

}
