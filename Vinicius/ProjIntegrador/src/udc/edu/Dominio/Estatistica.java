package udc.edu.Dominio;

public class Estatistica {
	private int temDeProcesso = 0;
	// estado do processo 0 pronto , 1 executando, 2 pausado, 3 excluido,4 fim
	private int Estado;
	public int getTemDeProcesso() {
		return temDeProcesso;
	}
	public void setTemDeProcesso(int temDeProcesso) {
		this.temDeProcesso = temDeProcesso;
	}
	public int getEstado() {
		return Estado;
	}
	public void setEstado(int estado) {
		Estado = estado;
	}
	
}
