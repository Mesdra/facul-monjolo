package udc.edu.Dominio;

import br.edu.udc.ed.vetor.Vetor;

public class ContextoDeSof {
	private int pid;
	// vetor de instrucoes
	private Vetor<Instrucoes> vetor = new Vetor<>();
	// posicao atual do vetor
	private int posica = 0;
	// tamanho do vetor
	private int tamanhoVetor = 0;
	
	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public Vetor<Instrucoes> getVetor() {
		return vetor;
	}

	public void setVetor(Vetor<Instrucoes> vetor) {
		this.vetor = vetor;
	}

	public int getPosica() {
		return posica;
	}

	public void setPosica(int posica) {
		this.posica = posica;
	}

	public int getTamanhoVetor() {
		return tamanhoVetor;
	}

	public void setTamanhoVetor(int tamanhoVetor) {
		this.tamanhoVetor = tamanhoVetor;
	}


	public Prioridade getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(Prioridade prioridade) {
		this.prioridade = prioridade;
	}


	// prioridade do processo 0 baixa, 1 media, 2 alta
	private Prioridade prioridade;
	
	public String toString() {
		return String.format("[pid = %d,posicao = %d,tamanhoV = %d, prioridade = %d]vetor = {%s}", this.pid,
				this.posica, this.tamanhoVetor, this.prioridade, this.vetor);
	}
}
