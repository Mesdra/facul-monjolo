package br.edu.udc.ed.vetor.alunos;

import java.util.Arrays;

public class VetorAlunos {
	private int quantidade = 0;
	private Aluno[] alunos =new  Aluno[100];
	
	public void adicionar(Aluno aluno){
		this.verificaCapacidade();
		
		this.alunos[quantidade] =aluno;
		this.quantidade++;
		
	}
	
	private void verificaCapacidade(){
		if(this.quantidade == this.alunos.length){
			final Aluno[] novoArray = new Aluno[this.alunos.length *2];
			
			for(int i=0;i<this.alunos.length;i++){
				novoArray[i] = this.alunos[i]; 
			}
			this.alunos = novoArray;
		}
		
		
	}
	
	
	
	public void adicionar(int posicao,Aluno aluno){
		if(!this.posicaoOcupada(posicao) && posicao != this.quantidade ){
			
			throw new IndexOutOfBoundsException("posicao invalida");
		}
		this.verificaCapacidade();
		for(int i = this.quantidade-1; i>posicao;i-=1){
			this.alunos[i+1]=this.alunos[i];
			
		}
		this.alunos[posicao] = aluno;
		this.quantidade++;
		
	}
	
	private boolean posicaoOcupada(int posicao){
		return posicao >= 0 &&  posicao < this.quantidade;
		
	}
	
	
	public Aluno obtem (int posicao){
		if(!this.posicaoOcupada(posicao))
			throw new IndexOutOfBoundsException("posicao invalida");
		return this.alunos[posicao];
	}
	
	
	
	
	
	public void remove (int posicao){
		if(!this.posicaoOcupada(posicao)){
			throw new  IndexOutOfBoundsException("posicao invalida");
		}
		for(int i=posicao;i<this.quantidade-1;i++){
			this.alunos[i] = this.alunos[i+1];
			
		}
		this.quantidade--;
		
	}
	
	
	
	
	public boolean contem(Aluno aluno){
		for(int i=0 ; i < this.quantidade; i++)
			if(aluno.equals (this.alunos[i])){
				return true;
			}
		return false;
	}
	public int tamanho(){
		return this.quantidade;
	}
	public String toString(){
		return Arrays.toString(this.alunos);
	}

}
