package br.edu.udc.ed.vetor.Test;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.vetor.VetorObject;
import br.edu.udc.ed.vetor.alunos.Aluno;




public class VetorObjectTest {

		@Test
		public void adicionaDevePassar(){
			final VetorObject vetor= new VetorObject();
			vetor.adicionar(new Aluno());
			vetor.adicionar("uma string");
			vetor.adicionar(1);
			vetor.adicionar(System.out);
			Assert.assertEquals(4,vetor.tamanho());
			
}       
		@Test
		public void adicionaDevePassaGenerica(){
			final VetorObject vetor= new VetorObject();
			vetor.adicionar("String");
			vetor.adicionar(1L);
			vetor.adicionar('c');
			
			//final char c = (char) vetor.obtem(2);
			
			
			
		}
}