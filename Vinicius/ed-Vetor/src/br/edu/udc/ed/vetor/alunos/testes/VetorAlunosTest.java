package br.edu.udc.ed.vetor.alunos.testes;

import org.junit.Test;
import org.junit.Assert;

import br.edu.udc.ed.vetor.alunos.Aluno;
import br.edu.udc.ed.vetor.alunos.VetorAlunos;


public class VetorAlunosTest {
	@Test
	public void adicionaCom2Alunos(){
		final  VetorAlunos vetorAlunos =new VetorAlunos();
		final Aluno maria = new Aluno();
		maria.nome = "maria joaquina";
		maria.creditos = 100;
		vetorAlunos.adicionar(maria);
		
		final Aluno jose = new Aluno();
		jose.nome = "jose mariano";
		jose.creditos = 50;
		vetorAlunos.adicionar(jose);
		
		Assert.assertEquals(2,vetorAlunos.tamanho());
		Assert.assertTrue(vetorAlunos.contem(maria));
		Assert.assertTrue(vetorAlunos.contem(jose));
	}
	@Test 
	
	public void equalsDoAluno(){
		final Aluno m1 = new Aluno();
		
		m1.nome = "maria";
		
		System.out.println(m1);
		
		final Aluno m2 = new Aluno();
		
		m2.nome = "maria";
		
		System.out.println(m2);
		Assert.assertEquals(m1,m2);
		//usa a funcao equals para saber se duas variaveis sao iguais
	}
	
	public void equalsReferenciaEmMemoria(){
		final Aluno m1 = new Aluno();
		
		m1.nome = "maria";
		
		System.out.println(m1);
		
		final Aluno m2 = m1;
	
		Assert.assertTrue(m1==m2);
		// meche com o endereco de memoria
	}
	
	@Test
	
	public void converterDevePassar(){
		final Aluno m1 = new Aluno();
		m1.nome = "maria";
		
	final VetorAlunos vetor=new VetorAlunos();
	
	vetor.adicionar(m1);
	
		Assert.assertTrue(vetor.contem(m1));
		
	final Aluno m2 = new Aluno();
		m2.nome = "mariana";
		
		Assert.assertFalse(vetor.contem(m2));
		
	}
	
	@Test
	
	public void obtemDevePassar(){
		final VetorAlunos vetor = new VetorAlunos();
		
		final Aluno aluno0 = new Aluno();
		aluno0.nome = "aluno 0";
		vetor.adicionar(aluno0);
		
		final Aluno aluno1 = new Aluno();
		aluno1.nome = "aluno 1";
		vetor.adicionar(aluno1);
		
		final Aluno aluno2 = new Aluno();
		aluno2.nome = "aluno 2";
		vetor.adicionar(aluno2);
		
		Assert.assertEquals(aluno0, vetor.obtem(0));
		Assert.assertEquals(aluno1, vetor.obtem(1));
		Assert.assertEquals(aluno2, vetor.obtem(2));
		
	}
	
	@Test (expected=IndexOutOfBoundsException.class)
	
	public void obtemDeveFalharComPosicaoInvalida(){
		final VetorAlunos vetor = new VetorAlunos();
		
		vetor.adicionar(new Aluno());
		
		vetor.obtem(1000);
		Assert.fail("nao deveria retornar o aluno com posicao errada");
		
		
	}
	
@Test (expected=IndexOutOfBoundsException.class)
	
	public void obtemDeveFalharComPosicaoNegativa(){
		final VetorAlunos vetor = new VetorAlunos();
		
		vetor.adicionar(new Aluno());
		
		vetor.obtem(-100);
		Assert.fail("nao deveria retornar o aluno com posicao errada");
		
		
	}
@Test 
public void adicionaNaPosicaoDevePassar(){
	final VetorAlunos vetor = new VetorAlunos();
	
	vetor.adicionar( new Aluno());//00
	vetor.adicionar( new Aluno());//01
	vetor.adicionar( new Aluno());//02
	vetor.adicionar( new Aluno());//03
	
	final Aluno aluno = new Aluno();
	aluno.nome = "aluno 4";
	vetor.adicionar(4, aluno);//04
	
	Assert.assertEquals(aluno, vetor.obtem(4));
	Assert.assertEquals(5, vetor.tamanho());
	
	
}
@Test(expected=IndexOutOfBoundsException.class)
public void adicionaPossiçaoInvalidaDeveFalhar(){
final VetorAlunos vetor = new VetorAlunos();
	
	vetor.adicionar( new Aluno());//00
	vetor.adicionar( new Aluno());//01
	vetor.adicionar( new Aluno());//02
	vetor.adicionar( new Aluno());//03
	
	vetor.adicionar(10,new Aluno());//04
	
	Assert.fail("nao deveria adicionar");
	
	
	
	
}

@Test
public void adicionaNoMeioDevePassar(){
final VetorAlunos vetor = new VetorAlunos();
	
	vetor.adicionar( new Aluno());//00
	vetor.adicionar( new Aluno());//01
	vetor.adicionar( new Aluno());//02
	vetor.adicionar( new Aluno());//03
	
	final Aluno aluno = new Aluno();
	aluno.nome="aluno na possicao";
	vetor.adicionar(2,aluno);//04
	
	Assert.assertEquals(aluno,vetor.obtem(2));
	Assert.assertEquals(5,vetor.tamanho());
	
	
	
}
@Test 

public void removeDevePassar(){
	final VetorAlunos vetor =new VetorAlunos();
	
	vetor.adicionar( new Aluno());//0
	vetor.adicionar( new Aluno());//1
	vetor.adicionar( new Aluno());//2
	vetor.adicionar( new Aluno());//3
	vetor.adicionar( new Aluno());//4
	
	final Aluno aluno = new Aluno();
	
	aluno.nome = " Aluno a ser removido";
	vetor.adicionar(new Aluno());
	
	vetor.remove(5);
	
	Assert.assertFalse(vetor.contem(aluno));
	Assert.assertEquals(5,vetor.tamanho());
	
	
	
}
@Test 

public void removeNoMeioDevePassa(){
	final VetorAlunos vetor =new VetorAlunos();
	

	final Aluno aluno = new Aluno();
	
	aluno.nome = " Aluno na posicao 2";
	
	vetor.adicionar( new Aluno());//0
	vetor.adicionar( new Aluno());//1
	vetor.adicionar( aluno);//2
	vetor.adicionar( new Aluno());//3
	vetor.adicionar( new Aluno());//4

	
	vetor.remove(2);
	
	Assert.assertFalse(vetor.contem(aluno));
	Assert.assertEquals(4,vetor.tamanho());
	
	
	
}		
@Test		
public void adicionarSemLimite(){
	final VetorAlunos vetor= new VetorAlunos();
	
	for(int i=0;i < 99999;i++)
		vetor.adicionar(new Aluno());
	
	Assert.assertEquals(99999,vetor.tamanho());
}
	
	
		
}
