package br.edu.udc.ed.tabela;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.tabela.veiculo.Veiculo;
import br.edu.udc.ed.vetor.Vetor;

public class tabelaObjectTeste {
	@Test
	public void adicionaDevePassar() {
		final TabelaObject tabela = new TabelaObject();
		tabela.adiciona("uma string");
		tabela.adiciona(new Veiculo());
		tabela.adiciona(1);
		tabela.adiciona(1L);
		tabela.adiciona(1F);
		tabela.adiciona(1D);

		Assert.assertEquals(6, tabela.tamanho());

	}

	@Test
	public void testarGeneralizacao() {
		final TabelaObject tabela = new TabelaObject();
		tabela.adiciona("uma string");
		tabela.adiciona(1D);

		final Vetor<Object> objetos = tabela.todas();
		for (int i = 0; i < tabela.tamanho(); i++) {
				final Object objeto = objetos.obtem(i);
				if( objeto instanceof String){
					System.out.println("e uma string - >"+ objeto);
				}
				if( objeto instanceof Double){
					System.out.println("e uma double - >"+ objeto);
				}
		}

	}
}
