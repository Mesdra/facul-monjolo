package br.edu.udc.ed.tabela;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.vetor.Vetor;

public class TesteDicionario {
	
	@Test
	public void adicionaDevePassar(){
		final Dicionario dicionario = new Dicionario();
		
		dicionario.adiciona("Abacaxi");
		dicionario.adiciona("Uva");
		dicionario.adiciona("Pessego");
		dicionario.adiciona("Melao");
		
		Assert.assertEquals(4, dicionario.tamanho());
		Assert.assertTrue(dicionario.contem("Abacaxi"));

		
	}
	@Test
	public void adicionaDoisIguais(){
final Dicionario dicionario = new Dicionario();
		
		dicionario.adiciona("udc");
		dicionario.adiciona("udc");
		
		
		Assert.assertEquals(1, dicionario.tamanho());
		Assert.assertTrue(dicionario.contem("udc"));
		
	}
	@Test
	public void adicionaMaiusculoMinusculo(){
		final Dicionario dicionario = new Dicionario();
		
		dicionario.adiciona("udc");
		dicionario.adiciona("UDC");
		
		
		Assert.assertEquals(2, dicionario.tamanho());
		Assert.assertTrue(dicionario.contem("udc"));
		Assert.assertTrue(dicionario.contem("UDC"));
		
	}
	@Test
	public void removeDevePassar(){
		final Dicionario dicionario = new Dicionario();
		
		dicionario.adiciona("uva");
		dicionario.adiciona("pessego banana");
		dicionario.adiciona("banana");
		
		dicionario.remove("uva");
		
		assertEquals(2,dicionario.tamanho());
		assertFalse(dicionario.contem("uva"));
		
	}
	@Test(expected=RuntimeException.class)
	public void removeDeveFalhar(){
		final Dicionario dicionario = new Dicionario();
		
		dicionario.remove("nao Existe");
		Assert.fail("nao pode passar");
	}
	
	@Test(expected=RuntimeException.class)
	public void removeDeveFalharremovendo2vezes(){
		final Dicionario dicionario = new Dicionario();
		
		dicionario.adiciona("nao Existe");
		dicionario.remove("nao Existe");
		dicionario.remove("nao Existe");
		
		Assert.fail("nao pode passar");
	}
	@Test
	public void listarTodas(){
		final Dicionario dicionario = new Dicionario();
		
		dicionario.adiciona("uva");
		dicionario.adiciona("pera");
		dicionario.adiciona("abacaxi");
		dicionario.adiciona("teste");
		dicionario.adiciona("ssssss");
		
		Assert.assertEquals(5, dicionario.tamanho());
		
		final Vetor<String> palavras = dicionario.todas();
		Assert.assertEquals(5, palavras.tamanho());
		
		System.out.println(palavras);
		
	}
	@Test
	public void Lancamento(){
		final Dicionario dicionario = new Dicionario();
		
		for(int i=0;i<99999;i++){
			dicionario.adiciona(new BigInteger(20,new SecureRandom()).toString(32) );
			
		}
		
		dicionario.imprimir();
		
	}
	@Test
	public void remove(){
		final Dicionario dicionario = new Dicionario();
		
		for(int i=0;i<99999;i++){
			dicionario.adiciona(new BigInteger(20,new SecureRandom()).toString(32) );
			
		}
		final Vetor<String> palavras = dicionario.todas();
		for(int i=0;i<99999;i++){
			dicionario.remove(palavras.obtem(i));	
		}
		Assert.assertEquals(0,dicionario.tamanho());
		
		dicionario.imprimir();
		
	}
	
}
