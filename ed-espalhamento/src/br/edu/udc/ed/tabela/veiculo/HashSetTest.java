package br.edu.udc.ed.tabela.veiculo;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class HashSetTest {

	@Test
	public void adicionaDevePassar() {
		final Set<String> palavras = new HashSet<String>();

		palavras.add("udc");
		palavras.add("monjolo");
		palavras.add("teste");
		palavras.add("carro");

		Assert.assertEquals(4, palavras.size());
		Assert.assertTrue(palavras.contains("udc"));

		final Object[] palavrasArray = palavras.toArray();
		for (int i = 0; i < palavrasArray.length; i++)
			System.out.println(palavrasArray[i]);
		for (String palavra : palavras) {
			System.out.println(palavras);
		}

		palavras.forEach(palavra -> {
			System.out.println(palavra);
		});

		palavras.stream().forEach(palavra -> {
			System.out.println(palavras);
		});
	}

	@Test
	public void removeDevaPassar() {
		final Set<Veiculo> veiculo = new HashSet<>();

		veiculo.add(new Veiculo());
		veiculo.add(new Veiculo());
		veiculo.add(new Veiculo());
		veiculo.add(new Veiculo());
		
		final Veiculo c4  = new Veiculo();
		veiculo.add(c4);
		
		
		Assert.assertEquals(1, veiculo.size());
	}
}
