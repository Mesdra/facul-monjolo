package br.edu.udc.ed.tabela.veiculo;

public class Veiculo {
	private String modelo;
	private String cor;
	private Boolean conversivel;
	private Float peso;

	public int hashCode() {
		final int primo = 31;
		int codigo = 1;
		if (this.cor != null) {
			codigo = primo * codigo + this.cor.hashCode();
		}
		if (this.conversivel != null) {
			codigo = primo * codigo + this.conversivel.hashCode();
		}
		if (this.modelo != null) {
			codigo = primo * codigo + this.modelo.hashCode();
		}
		if (this.peso != null) {
			codigo = primo * codigo + this.peso.hashCode();
		}
		return codigo;

	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Boolean getConversivel() {
		return conversivel;
	}

	public void setConversivel(Boolean conversivel) {
		this.conversivel = conversivel;
	}

	public Float getPeso() {
		return peso;
	}

	public void setPeso(Float peso) {
		this.peso = peso;
	}

}
