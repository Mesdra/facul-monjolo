package br.edu.udc.ed.tabela.veiculo;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class VeiculoTeste {
	@Test
	public void testHascCode() {
		System.out.println(new Veiculo().hashCode());
		System.out.println(new Veiculo().hashCode());
		System.out.println(new Veiculo().hashCode());
		System.out.println(new Veiculo().hashCode());

	}

	@Test
	public void testHasCode2DeveSerIgual() {
		final Veiculo veiculo1 = new Veiculo();
		veiculo1.setCor("azul");
		veiculo1.setConversivel(false);

		final Veiculo veiculo2 = new Veiculo();
		veiculo2.setCor("azul");
		veiculo2.setConversivel(false);

		Assert.assertEquals(veiculo1.hashCode(), veiculo2.hashCode());
	}

	// usar no trabalho
	@Test
	public void testeImprecaohasCode() {
		final Veiculo veiculo1 = new Veiculo();
		veiculo1.setCor("azul");
		veiculo1.setConversivel(false);
		veiculo1.setModelo("brazilia");
		System.out.println(veiculo1);

		final Veiculo veiculo2 = new Veiculo();
		veiculo2.setCor("azul");
		veiculo2.setConversivel(false);
		veiculo2.setModelo("outro carro");
		System.out.println(veiculo2);
	}

}
