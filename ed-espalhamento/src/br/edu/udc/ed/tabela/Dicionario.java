package br.edu.udc.ed.tabela;

import br.edu.udc.ed.vetor.Vetor;

public class Dicionario {
	private Vetor<Vetor<String>> tabela = new Vetor<Vetor<String>>();
	private static final int QUANTIDADE_LETRAS = 10;
	private int quantidade = 0;

	public Dicionario() {
		for (int i = 0; i < QUANTIDADE_LETRAS; i++) {
			final Vetor<String> lista = new Vetor<String>();
			this.tabela.adicionar(lista);
		}
	}

	private void redimencionarTabela(int novaCapacidade) {
		final Vetor<String> palavras = this.todas();
		this.tabela = new Vetor<>();

		for (int i = 0; i < novaCapacidade; i++) {
			this.tabela.adicionar(new Vetor<String>());
		}

		for (int i = 0; i < palavras.tamanho(); i++) {
			final String palavra = palavras.obtem(i);
			final int indice = this.calculaIndice(palavra);
			this.tabela.obtem(indice).adicionar(palavra);
		}
	}

	private void verificaCapacidade() {
		int capacidade = this.tabela.tamanho();
		final int carga = this.tamanho() / capacidade;

		if (carga >= 0.75) {
			this.redimencionarTabela(capacidade * 2);
		}
		if (carga <= 0.25) {
			final int novaCapacidade = capacidade / 2;
			if (novaCapacidade > QUANTIDADE_LETRAS) {
				this.redimencionarTabela(novaCapacidade);
			}

		}

	}

	private int calculaIndice(String palavra) {
		int codigoDeEspalhamento = this.gerarCodigo(palavra);
		if (codigoDeEspalhamento < 0) {
			codigoDeEspalhamento *= -1;
		}
		return codigoDeEspalhamento % this.tabela.tamanho();
	}

	private int gerarCodigo(String palavra) {
		int codigo = 1;
		// length pega a quantidade de caracteres
		for (int i = 0; i < palavra.length(); i++) {
			// letra por letra
			codigo = 31 * codigo + palavra.charAt(i);
		}
		return codigo;
	}

	public void remove(String palavra) {
		if (this.contem(palavra)) {
			final int indice = this.calculaIndice(palavra);
			final Vetor<String> lista = this.tabela.obtem(indice);
			for (int i = 0; i < lista.tamanho(); i++) {
				if (lista.obtem(i).equals(palavra)) {
					lista.remove(i);
					break;

				}
			}
			this.quantidade--;
			this.verificaCapacidade();
		} else {
			throw new RuntimeException("PALAVRA NAO EXISTE");
		}
	}

	public void adiciona(String palavra) {
		if (!this.contem(palavra)) {
			this.verificaCapacidade();

			final int indice = this.calculaIndice(palavra);
			this.tabela.obtem(indice).adicionar(palavra);
			this.quantidade++;

		}
	}

	public boolean contem(String palavra) {
		final int indice = this.calculaIndice(palavra);
		return this.tabela.obtem(indice).contem(palavra);
	}

	public Vetor<String> todas() {
		final Vetor<String> todasPalavras = new Vetor<String>();

		for (int i = 0; i < this.tabela.tamanho(); i++) {
			final Vetor<String> palavras = this.tabela.obtem(i);

			for (int j = 0; j < palavras.tamanho(); j++) {
				final String palavra = palavras.obtem(j);
				todasPalavras.adicionar(palavra);
			}
		}
		return todasPalavras;
	}

	public int tamanho() {

		return this.quantidade;
	}

	public void imprimir() {
		
		System.out.println("quantidade"+this.tabela.tamanho());
		for (int i = 0; i < this.tabela.tamanho(); i++) {
			
			final Vetor<String> palavras = tabela.obtem(i);
			
			if( palavras.tamanho() == 0) continue;
			System.out.println("o codigo ;" + i + " total " + palavras.tamanho());
			
			for (int j = 0; j < palavras.tamanho(); j++) {
				// System.out.println(palavras.obtem(j));

			}
		}

	}

}
